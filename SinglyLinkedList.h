typedef struct Student student;
char *pAssess[] = {"Medium", "High", "Excellent"};

/**
* Adds a student to the list
* @param p 0 for adding at the beginning, 1 for adding at the end
* @param name of the student
* @param matNum of the student
* @param eDay enrollment day of the student
* @param eMon enrollment month of the student
* @param eYear enrollment year of the student
* @param gP
* @param pAssess
*/
void addStudent(int p, char *name, int matNum, int eDay, int eMon, int eYear, int gP, char *pAssess);

/**
 * Deletes a student
 * @param matNum of the student
 */
void delStudent(int matNum);

/**
 * Prints the list
 */
void printStudents(void);

/**
 * Adds grading points to a student
 * @param matNum of the student
 * @param gradingPoints amount that is going to be added
 */
void addPoints(int matNum, int gradingPoints);

/**
 * Enters an assessment of a student, high if he has over 1800 and excellent if he is the best.
 * @param pAssess The assessment
 * @return The matNum of the student with the highest grading points and if it's lower than 1800 it returns -1
 */
int assessStudents(char **pAssess);

/**
 * Deletes students that have less or the equal amount of grading Points
 * @param gradingPoints The grading points we are checking against
 */
void deleteStudentsWithGradingPointsEqualOrLowerThan(int gradingPoints);
