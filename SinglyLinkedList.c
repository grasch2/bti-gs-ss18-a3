#include <stddef.h> // for NULL
#include <string.h> // for strcpy
#include <stdlib.h> // for malloc
#include <stdio.h> // for printf
#include "TI_Lib.h" // for TI board methods
#include "tft.h" // for tft functions

struct Date {
    int day;
    int month;
    int year;
};

typedef struct Student {
    char name[20]; // last name
    int matNum;
    struct Date enrollment;
    int gradingPoints; // sum of the grading points, max 2000
    char *pAssess; // assessment to the grading
    struct Student *next;
} student;

student *head = NULL;

/**
* Es wird ein s vom Typ Student erstellt (malloc)
* 1. Falls die Liste leer ist, ist das einziege Element s
* 2. (Vorne) Liste mit min. 1 Element:
*    An s Next-Verkettung von NULL l�sen und an das erste Element(head) verketten
* 3. Das erste Element aktualisieren
* 4. (Hinten) Liste mit min. 1 Element:
*    Neue Variable worker, um an die letzte Position zu kommen
*    Das letzte Element Next-Verkettung hat NULL somit Abbruchbedingung
* 5. Anstatt NULL bekommt es die neue Verkettung mit s
*/
void addStudent(int p, char *name, int matNum, int eDay, int eMon, int eYear, int gP, char *pAssess) {
    student *s = NULL;
    s = malloc(sizeof(student));
    strcpy(s->name, name);
    s->matNum = matNum;
    s->enrollment.day = eDay;
    s->enrollment.month = eMon;
    s->enrollment.year = eYear;
    s->gradingPoints = gP;
    s->pAssess = &pAssess[0];
    s->next = NULL;

    // check if list is empty
    if (head == NULL) {
        head = s; // 1.
    } else if (p == 0) { // add to first position
        s->next = head; // 2.
        head = s; // 3.
    } else { // add to last position
        student *worker = head;
        while (worker->next != NULL) { // 4. go to last entry on list
            worker = worker->next;
        }
        worker->next = s; // 5. set last entries next to our new student
    }
}

void delStudent(int matNum) {
    if (head != NULL) { // check if list is empty
        student *worker = head;
        student *prevWorker = head;
		 	while (worker->matNum != matNum) { // run until the matNum is identifie: find student with the matNum we are searching for
            if (worker->next != NULL) { // only change worker if we haven't reached the end of the list
                prevWorker = worker;
                worker = worker->next;
            } else { // if we have reached the end of the list then there is no student with that matNum
                return; // so we do nothing
            }
        }
        // set the student from before the one we are going to delete to point at the student
        // after the one we are going to delete him
        prevWorker->next = worker->next; // 3. vorherige verkettung aufloesen, neue verkettung erzeugen
        free(worker); // free the memory used by the now deleted student struct
    }
}

/**
* 1. Liste mit min. 1 Element wird gedruckt:
*    Neue Variable worker, um alle Elemente 1-mal anzufassen
*    Jedes Element existiert, somit ist NULL: Abbruchbedingung
*/
void printStudents(void) {
    if (head != NULL) { // only if list is not empty
        student *worker = head;
        while (worker != NULL) { // 1. go to each node and print the information of the student
            printf("Name: %s\n", worker->name);
            printf("Matriculation Number: %d\n", worker->matNum);
            printf("Enrollment Date: %04d-%02d-%02d\n", worker->enrollment.year, worker->enrollment.month, worker->enrollment.day);
            printf("Grading Points: %d\n", worker->gradingPoints);
            printf("Assessment: %s\n\n", worker->pAssess);

						char temp[40] = "";
						sprintf(temp, "Name: %s ( %d )", worker->name, worker->matNum);
						TFT_puts(temp);
						TFT_newline();
						TFT_carriage_return();
						sprintf(temp, "Enrollment Date: %d-%d-%d", worker->enrollment.year, worker->enrollment.month, worker->enrollment.day);
						TFT_puts(temp);
						TFT_newline();
						TFT_carriage_return();
						sprintf(temp, "Grading Points: %d ( %s )", worker->gradingPoints, worker->pAssess);
						TFT_puts(temp);
						TFT_newline();
						TFT_carriage_return();
					
            worker = worker->next;
        }
    }
}

/**
*    Liste mit min. 1 Element werden aktualisiert:
* 1. Neue Variable worker, um an den Student mit der Matrikelnr. zu kommen
* 2. Die gesuchte Matrikelnr. ist matNum somit Abbruchbedingung
* 3. Wenn Liste zuende, passiert nichts
* 4. Dem student wird die Punkte hinzugef�gt, aber nicht �ber 2000
*/
void addPoints(int matNum, int gradingPoints) {
    if (head != NULL) { // only if list is not empty
        student *worker = head; //1.
        while (worker->matNum != matNum) { // 2. find student with the matNum
            if (worker->next != NULL) { 
                worker = worker->next;
            } else { // 3. if we have not found him 
                return;
            }
        }
        // 4. check if the grading points do not exceed 2000 and if they do set them to 2000
        int newGradingPoints = worker->gradingPoints + gradingPoints;
        if (newGradingPoints > 2000) {
            newGradingPoints = 2000;
        }
        worker->gradingPoints = newGradingPoints;
    }
}

/**
*    Liste mit min. 1 Element werden aktualisiert:
* 1. Neue Variable bestStudent, um den besten Studenten zu finden 
* 2. Neue Variable worker, um alle Elemente 1-mal anzufassen
* 3. Das letzte Element Next-Verkettung hat NULL somit Abbruchbedingung
* 4. Durch vergleichen den bestenStudent herausfiltern
* 5. Den Studenten werden ab 1800 Punkten in High umgewandelt
* 6. Der beste Student ab 1800 Punkten bekommt Excellent und wird returnt
*    wenn er unter 1800 Punkten hat wird -1 zur�ck gegeben
*/
int assessStudents(char **pAssess) {
    if (head != NULL) { // only if list is not empty
        student *bestStudent = head; // 1.
        student *worker = head; // 2.
        while (worker != NULL) { // 3. go through the whole list
            // 4. find the student with the highest grading points and set him to bestStudent
            if (worker->gradingPoints > bestStudent->gradingPoints) {
                bestStudent = worker;
            }
            // 5. give every student with grading points above or equally to 1800 the parameter assessment string pointer
            if (worker->gradingPoints >= 1800) {
                worker->pAssess = pAssess[1];
            }
            worker = worker->next;
        }
        // 6. give the best student with grading points above or equally to 1800 the next parameter assessment string pointer
        if (bestStudent->gradingPoints >= 1800) {
					bestStudent->pAssess = pAssess[2]; // find the last null terminator and return its adress and increment it:
					// pointer to the following string
            return bestStudent->matNum;
        }
    }
    return -1; // if we could not find a best student we return this instead
}

/**
*    Liste mit min. 1 Element werden aktualisiert:
* 1. Neue Variable sizeOfArray, um die gr��e zu bestimmen f�r das malloc
* 2. Neue Variable worker, um alle Elemente 1-mal anzufassen
* 3. Das letzte Element Next-Verkettung hat NULL somit Abbruchbedingung
*    Diese Schleife ist nur daf�r da, die Anzahl der zu l�schenden Studenten zu finden
* 4. Neue Variable students, um die Elemente abzuspeichern, die zu l�schen sind
* 5. Neue Variable i, um durch zu iterieren
* 6. Das letzte Element Next-Verkettung hat NULL somit Abbruchbedingung
*    Diese Schleife ist daf�r da, um die studenten, die gel�scht werden sollen, in das Array abzuspeichern
* 7. Aus dem Array, der zu l�schenden Studenten, wird die matNum an die delStudent() �bergeben,
*    um entg�ltig zu l�schen
*/
void deleteStudentsWithGradingPointsEqualOrLowerThan(int gradingPoints) {
    if (head != NULL) { // only if list is not empty
        // first iterate through list and find how many students are affected
        int sizeOfArray = 0; // 1.
        student *worker = head; // 2. 
        while (worker->next != NULL) { // 3. go through the whole list
            if (worker->gradingPoints <= gradingPoints) {
                sizeOfArray++; // if we found an affected student increase the size
            }
            worker = worker->next;
        }
        // second create an array with the determined size and iterate
        // through the list again adding every affected user to the array
        student **students = NULL;
        students = malloc(sizeOfArray * sizeof(student*)); // 4.
        int i = 0; // 5.
        worker = head;
        while (worker->next != NULL) { // 6. go through the whole list
            if (worker->gradingPoints <= gradingPoints) {
                students[i++] = worker;
            }
            worker = worker->next;
        }
        // 7. now delete every user that is in that array
        for (int j = 0; j < sizeOfArray; j++) {
            delStudent(students[j]->matNum);
        }
        free(students); // free the space we allocated for the array
    }
}
