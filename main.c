#include <stdio.h> // for printf
#include "TI_Lib.h" // for TI board methods
#include "tft.h" // for tft functions
#include "SinglyLinkedList.h"

int main() {
		Init_TI_Board();
		TFT_cls();
    printf("Added Students:    ----------------------------------------\n");
		TFT_puts("Added Students:    ---------------------");
		Delay(2000);
		
    addStudent(1, "Mueller", 1, 9, 12, 2010, 0, pAssess[0]);
    addStudent(1, "Schmidt", 2, 10, 11, 2011, 0, pAssess[0]);
    addStudent(1, "Schneider", 3, 11, 10, 2012, 0, pAssess[0]);
    addStudent(1, "Fischer", 4, 12, 9, 2013, 0, pAssess[0]);
    addStudent(1, "Weber", 5, 13, 8, 2014, 0, pAssess[0]);
    addStudent(1, "Meyer", 6, 14, 7, 2015, 0, pAssess[0]);
    addStudent(1, "Wagner", 7, 15, 6, 2016, 0, pAssess[0]);
    addStudent(1, "Becker", 8, 16, 5, 2017, 0, pAssess[0]);
    printStudents();

		TFT_newline();
		TFT_carriage_return();
    printf("Added Points:    ----------------------------------------\n");
		TFT_puts("Added Points:    -----------------------");
		Delay(2000);
    addPoints(1, 1799);
    addPoints(3, 1800);
    addPoints(5, 1820);
    addPoints(8, 1830);
    printStudents();

		TFT_newline();
		TFT_carriage_return();
    printf("Added Assessment:    ----------------------------------------\n");
		TFT_puts("Added Assessment:    -------------------");
		Delay(2000);
    printf("Matriculation Number of the best student: %d\n\n", assessStudents(pAssess));
		char temp[10] = "";
		sprintf(temp, "Matriculation Number of best student: %d", assessStudents(pAssess));
		TFT_puts(temp);
		TFT_newline();
		TFT_carriage_return();
		Delay(2000);
    printStudents();

		TFT_newline();
		TFT_carriage_return();
    printf("Deleted Student with less than or equals 100 grading points:    ----------------------------------------\n");
		TFT_puts("Deleted Student with less than or equals 100 grading points:   -----------------");
		Delay(2000);
    deleteStudentsWithGradingPointsEqualOrLowerThan(100);
    printStudents();
    return 0;
}
